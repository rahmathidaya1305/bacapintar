<?php

use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UlasanController;
use App\Http\Controllers\KategoriController;
use App\Http\Controllers\NotFoundController;
use App\Http\Controllers\PeminjamanController;
use App\Http\Controllers\ReservationController;
use App\Http\Controllers\PengembalianController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Auth::routes();

// Route::get('/', function () {
//     return view('login');
// });
Route::get('/', function () {
    return view('auth.login');
});

Route::middleware('auth')->group(function () {
    Route::resources([
        'books' => BukuController::class,
        'category' => KategoriController::class,
        'peminjaman' => PeminjamanController::class,
        'pengembalian' => PengembalianController::class,
        'reservasi' => ReservationController::class,
        'ulasan' => UlasanController::class,
    ]);
    Route::get('/back/{id}', [PengembalianController:: class, 'returnBooks'])->name('return.back')->middleware('role:admin');
    Route::post('/back/{id}', [PengembalianController:: class, 'insertRecord'])->name('return.insert')->middleware('role:admin');
    Route::get('/back', [PengembalianController:: class, 'index'])->name('return.index')->middleware('role:admin');
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    Route::get('/404', [NotFoundController::class, 'index'])->name('404')->middleware('role:admin');
    Route::get('/user/{username}/{id}',[UserController::class, 'index'])->name('user.index')->middleware('role:admin');
    Route::put('/user/{username}/{id}',[UserController::class, 'update'])->name('user.update')->middleware('role:admin');
});


