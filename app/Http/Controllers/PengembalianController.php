<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\PeminjamanModel;
use App\Models\PengembalianModel;

class PengembalianController extends Controller
{
    public function index()
    {
        $kembali = PengembalianModel::all();
        return view('pengembalian.index',compact('kembali'));
    }
    public function returnBooks(string $id)
    {
        // dd($id);
        $peminjaman = PeminjamanModel::find($id);
        $peminjaman->denda = $peminjaman->calculate();
        $peminjaman->Status();
        $peminjaman->update();
        return view('pengembalian.detail',compact('peminjaman'));
    }

    public function insertRecord(Request $request , string $id)
    {
        // ini untuk memperbarui status pada table peminjaman khusus status saja
        $peminjaman = PeminjamanModel::find($id);
        $peminjaman->status = 'dikembalikan';
        $peminjaman->update();

        // ini untuk menambahkan data pada table pengembalian
        $kembali = new PengembalianModel();
        $kembali->id_peminjaman = $request->input('id_peminjam');
        $kembali->tgl_pengembalian = $request->input('tglkembali');
        $kembali->harga_sewa = $request->input('sewa');
        $kembali->denda = $request->input('denda');
        $kembali->total_bayar = $request->input('totalbayar');
        $kembali->status = 'dikembalikan';
        $kembali->save();
        return redirect()->route('pengembalian.index')->with('Buku Berhasil dikembalikan');
    }
}
