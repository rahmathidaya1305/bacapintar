<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index(string $username, $id_user)
    {
        // Ambil nama dan juga id_user
        $user = User::where('username',$username)->where('id_user',$id_user)->firstOrFail();
        return view('profile.index',compact('user'));
    }

    public function update(Request $request, string $username,$id_user)
    {

        // Ambil nama dan juga id_user
        $user = User::where('username',$username)->where('id_user',$id_user)->firstOrFail();
        if($request->has('photo')){
            $photo = $request->file('photo');
            $namafile = time().'.'.$photo->getClientOriginalExtension();
            $photo->move('assets/image/',$namafile);
            if($user->avatar){
                $oldFile = public_path('assets/image/' . $user->avatar);
                if (file_exists($oldFile)) {
                    unlink($oldFile);
                }
            }
            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
            $user->avatar = $namafile;
        }else{
            $user->name = $request->input('name');
            $user->username = $request->input('username');
            $user->email = $request->input('email');
        }
        $user->update();
        return redirect()->back()->with('success','Profile Berhasil Diperbaharui');
    }
}
