<?php

namespace App\Http\Controllers;

use App\Models\BukuModel;
use App\Models\PeminjamanModel;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $peminjaman = PeminjamanModel::where('status', 'dipinjam')->get();
        return view('peminjaman.index', compact('peminjaman'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {

        $user = User::all();
        $buku = BukuModel::all();
        return view('peminjaman.form.create',  compact('user', 'buku'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // kurangi stok berdasarkan jumlah buku yang dipinjam
        $buku = BukuModel::findOrFail($request->buku);
        $buku->stok_buku -= $request->input('ttlbuku');
        $buku->update();
        // dd($buku->stok_buku);
        // if($buku->stok_buku < $request->ttlbuku ){
        //     return redirect()
        // }

        $peminjaman = new PeminjamanModel();
        $peminjaman->id_user = $request->input('user');
        $peminjaman->id_buku = $request->input('buku');
        $peminjaman->tgl_peminjaman = $request->input('tglpinjam');
        $peminjaman->tgl_pengembalian = $request->input('tglkembali');
        $peminjaman->harga = $request->input('hargasewa');
        $peminjaman->total_buku = $request->input('ttlbuku');
        $peminjaman->status = $request->input('status');
        $peminjaman->save();
        return redirect()->route('peminjaman.index')->with('success', 'Peminjaman Berhasil Dibuat');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $peminjaman = PeminjamanModel::findOrFail($id);
        $user = User::all();
        $buku = BukuModel::all();
        return view('peminjaman.form.edit', compact('peminjaman','user','buku'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $peminjaman = PeminjamanModel::findOrFail($id);
        $buku = BukuModel::findOrFail($request->buku);
        // tambahkan stok buku dari table peminjaman dan jika ada perubahan qty maka kurangi kembali dari
        // stok buku yang sudah diperbaharui

        $buku->stok_buku += $peminjaman->total_buku;
        $buku->stok_buku -= $request->input('ttlbuku');
        $buku->update();


        $peminjaman->id_user = $request->input('user');
        $peminjaman->id_buku = $request->input('buku');
        $peminjaman->tgl_peminjaman = $request->input('tglpinjam');
        $peminjaman->tgl_pengembalian = $request->input('tglkembali');
        $peminjaman->harga = $request->input('hargasewa');
        $peminjaman->total_buku = $request->input('ttlbuku');
        $peminjaman->status = $request->input('status');
        $peminjaman->denda = $peminjaman->calculate();
        $peminjaman->update();
        return redirect()->route('peminjaman.index')->with('success', 'Peminjaman Berhasil Diubah');


    }


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $kategori = PeminjamanModel::findOrFail($id);
        $kategori->delete();
        return redirect()->route('peminjaman.index')->with('success', 'Data peminjaman berhasil dihapus');
    }

}
