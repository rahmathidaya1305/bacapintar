<?php

namespace App\Http\Controllers;

use App\Models\BukuModel;
use App\Models\UlasanModel;
use App\Models\User;
use Illuminate\Http\Request;

class UlasanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $ulasan = UlasanModel::with(['user','buku'])->get();
        return view('ulasan.index', compact('ulasan'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $user = User::all();
        $buku = BukuModel::all();
        return view('ulasan.form-ulasan.create', compact('user', 'buku'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $request->validate([
            'user' => 'required',
            'buku' => 'required',
            'rating' => 'required',
            'komentar' => 'required',
        ]);
        $ulasan = new UlasanModel();
        $ulasan->id_user = $request->input('user');
        $ulasan->id_buku = $request->input('buku');
        $ulasan->rating = $request->input('rating');
        $ulasan->komentar = $request->input('komentar');
        $ulasan->tgl_ulasan = date('Y-m-d');
        $ulasan->save();
        return redirect()->route('ulasan.index')->with('success', 'Ulasan Berhasil diberikan');

    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $ulasan = UlasanModel::findOrFail($id);
        $user = User::all();
        $buku = BukuModel::all();
        return view('ulasan.form-ulasan.edit', compact('ulasan', 'user', 'buku'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $request->validate([
            'user' => 'required',
            'buku' => 'required',
            'rating' => 'required',
            'komentar' => 'required',
        ]);
        $ulasan = UlasanModel::findOrFail($id);

        $ulasan->id_user = $request->input('user');
        $ulasan->id_buku = $request->input('buku');
        $ulasan->rating = $request->input('rating');
        $ulasan->komentar = $request->input('komentar');
        $ulasan->tgl_ulasan = date('Y-m-d');
        $ulasan->update();
        return redirect()->route('ulasan.index')->with('success', 'Ulasan Berhasil diperbaharui');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $ulasan = UlasanModel::findOrFail($id);
        $ulasan->delete();
        return redirect()->route('ulasan.index')->with('success', 'Ulasan Berhasil dihapus');
    }
}
