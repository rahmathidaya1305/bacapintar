<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeminjamanModel extends Model
{
    use HasFactory;
    protected $table = 'peminjaman';
    protected $primaryKey = 'id_peminjaman';
    protected $guarded = 'id_peminjaman';
    protected $with = ['user', 'buku'];

    // Model Peminjaman akan memiliki relasi belongsTo ke model User dan Buku,
    // karena setiap peminjaman hanya dimiliki oleh satu pengguna dan satu buku
    public function user (){
        return $this->belongsTo(User::class, 'id_user');
    }

    public function buku (){
        return $this->belongsTo(BukuModel::class, 'id_buku');
    }

    public function calculate()
    {
        $tgl_kembali = Carbon::parse($this->tgl_pengembalian);
        $tgl_sekarang = Carbon::now();

        if($tgl_sekarang->greaterThan($tgl_kembali)){
            $selisih_hari = $tgl_sekarang->diffInDays($tgl_kembali);
            return $selisih_hari * 2000;
        }
        return 0;

    }
    public function Status()
    {
        $tgl_kembali = Carbon::parse($this->tgl_pengembalian);
        $tgl_sekarang = Carbon::now();
        if($tgl_kembali < $tgl_sekarang){
            $this->status = 'terlambat';
        }else{
            $this->status = 'dipinjam';
        }
        // $this->update();
    }

    public function pengembalian()
    {
        return $this->hasMany(PengembalianModel::class, 'id_pegembalian');
    }
}
