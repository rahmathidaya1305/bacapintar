<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PengembalianModel extends Model
{
    use HasFactory;
    protected $table = 'pengembalian_buku';
    protected $primaryKey = 'id_pengembalian';
    protected $guarded = 'id_pengembalian';
    public function peminjaman(){
        return $this->belongsTo(PeminjamanModel::class, 'id_peminjaman');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');

    }
}
