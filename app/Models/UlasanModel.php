<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UlasanModel extends Model
{
    use HasFactory;
    protected $table = 'ulasan';
    protected $primaryKey = 'id_ulasan';
    protected $guarded = 'id_ulasan';

    // Model Ulasan akan memiliki relasi belongsTo ke model User dan Buku,
    // karena setiap ulasan hanya dimiliki oleh satu pengguna dan satu buku
    public function user(){
        return $this->belongsTo(User::class, 'id_user');
    }
    public function buku(){
        return $this->belongsTo(BukuModel::class, 'id_buku');

    }
}
