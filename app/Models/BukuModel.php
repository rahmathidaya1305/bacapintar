<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BukuModel extends Model
{
    use HasFactory;
    protected $table = 'buku';
    protected $primaryKey = 'id_buku';
    protected $guarded = 'id_buku';
    public function Kategori(){
        return $this->belongsTo(KategoriModel::class, 'id_kategori');
    }

    // Model Buku juga akan memiliki relasi hasMany
    // ke model Ulasan, karena satu buku bisa memiliki banyak ulasan.
    public function ulasan(){
        return $this->hasMany(UlasanModel::class, 'id_ulasan','id_ulasan');
    }

    public function buku(){
        return $this->hasMany(PeminjamanModel::class, 'id_peminjaman','id_peminjaman');
    }
}
