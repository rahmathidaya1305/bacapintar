@section('title', 'Halaman Category')
@section('breadcumb-title', 'Kategori Buku')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-info btn-sm" href="{{ route('category.create') }}"><i class="fas fa-plus-circle"></i>
                        Tambah Kategori</a>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kategori as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->nama }}</td>
                                    <td>{{ $data->deskripsi }}</td>
                                    <td>
                                        <div class="btn-group dropleft">
                                            <button type="button" class="btn btn-primary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-eye"></i>
                                            </button>
                                            <div class="dropdown-menu bg-light">
                                                <a class="dropdown-item"  href="{{ route('category.edit' ,$data->id_kategori) }}"><i class="fas fa-edit" ></i> Ubah</a>
                                                <a onclick="event.preventDefault();
                                                document.getElementById('delete-category').submit()" class="dropdown-item" href="{{ route('category.destroy',$data->id_kategori) }}"><i class="fas fa-trash" ></i> Delete</a>
                                                <form id="delete-category" action="{{ route('category.destroy',$data->id_kategori) }}" role="form" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
