@section('title', 'Halaman Ubah Ulasan')
@section('breadcumb-title', 'Ubah Ulasan Baru')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card card-primary card-outline">
                <div class="card-header">
                    <h3 class="card-title font-weight-bold"><i class="fas fa-star"></i> Form Ubah Ulasan</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('ulasan.update',$ulasan->id_ulasan) }}" method="POST" role="form">
                        @csrf
                        @method('PUT')
                          <div class="col-5 mb-3">
                            <label for="user">User</label>
                            <select class="form-control select2 @error('user')
                                is-invalid
                            @enderror" name="user" id="user">
                                @foreach ($user as $data)
                                <option value="{{ $data->id_user }}"
                                @if ($data->id_user === $ulasan->id_user)
                                    @selected(true)
                                @endif>{{ ucwords($data->name) }}</option>
                                @endforeach
                            </select>
                            @error('user')
                            <div class="invalid-feedback">
                               {{$message}}
                            </div>
                            @enderror
                          </div>
                          <div class="col-5 mb-3">
                            <label for="buku">Judul Buku</label>
                            <select class="form-control select2 @error('buku')
                                is-invalid
                            @enderror" name="buku" id="buku">
                                @foreach ($buku as $data)
                                <option value="{{ $data->id_buku }}"
                                    @if ($data->id_buku === $ulasan->id_buku)
                                        @selected(true)
                                    @endif
                                    >{{ ucwords($data->judul) }}</option>
                                @endforeach
                            </select>
                            @error('buku')
                            <div class="invalid-feedback">
                               {{$message}}
                            </div>
                            @enderror
                          </div>
                          <div class="col-5 mb-3">
                            <label for="rating">Rating</label>
                            <select class="form-control @error('rating')
                                is-invalid
                            @enderror" name="rating" id="rating">
                            <option class="font-weight-bold" selected="true" value="{{ $ulasan->rating }}">
                                @if ($ulasan->rating == 1)
                                Sangat Buruk
                                @elseif ($ulasan->rating == 2)
                                Buruk
                                @elseif ($ulasan->rating == 3)
                                Cukup
                                @elseif ($ulasan->rating == 4)
                                Baik
                                @elseif ($ulasan->rating == 5)
                                Sangat Baik
                                @endif
                            </option>
                                <option value="1">1 - Sangat Buruk</option>
                                <option value="2">2 - Buruk</option>
                                <option value="3">3 - Cukup</option>
                                <option value="4">4 - Baik</option>
                                <option value="5">5 - Sangat Baik</option>
                            </select>
                            @error('rating')
                            <div class="invalid-feedback">
                               {{$message}}
                            </div>
                            @enderror
                          </div>
                          <div class="col-5 mb-3">
                            <label for="komentar">Komentar</label>
                            <textarea class="form-control @error('komentar')
                                is-invalid
                            @enderror" name="komentar" id="komentar" cols="30" rows="3">{{ $ulasan->komentar }}</textarea>
                            @error('komentar')
                            <div class="invalid-feedback">
                               {{$message}}
                            </div>
                            @enderror
                          </div>

                          <div class="mb-3">
                              <button type="reset" class="btn btn-warning"><i class="fas fa-sync-alt"></i> Hapus</button>
                            <button type="submit" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Buat</button>
                          </div>
                      </form>
                </div>
            </div>
        </div>
    </div>
@endsection
