@section('title', 'Halaman Ulasan')
@section('breadcumb-title', 'Data Ulasan')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-info btn-sm" href="{{ route('ulasan.create') }}"><i class="fas fa-plus-circle"></i>
                        Buat Ulasan</a>
                </div>
                <div class="card-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User</th>
                                <th>Buku</th>
                                <th>Rating</th>
                                <th>Komentar</th>
                                <th>Tgl. Ulasan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($ulasan as $rows)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $rows->user->name }}</td>
                                    <td>{{ $rows->buku->judul }}</td>
                                    <td>
                                        @if ($rows->rating == 1)
                                            <span class="badge badge-danger p-2">1 - Sangat Buruk</span>
                                        @elseif ($rows->rating == 2)
                                            <span class="badge badge-info p-2">2 - Buruk</span>
                                        @elseif ($rows->rating == 3)
                                            <span class="badge badge-warning p-2">3 - Cukup</span>
                                        @elseif ($rows->rating == 4)
                                            <span class="badge badge-primary p-2">4 - Baik</span>
                                        @elseif ($rows->rating == 5)
                                            <span class="badge badge-success p-2">5 - Sangat Baik</span>
                                        @endif
                                    </td>
                                    <td>{{ $rows->komentar }}</td>
                                    <td>{{ date('d-m-Y', strtotime($rows->tgl_ulasan)) }}</td>
                                    <td>
                                        <div class="btn-group dropleft">
                                            <button type="button" class="btn btn-primary dropdown-toggle"
                                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <i class="fas fa-eye"></i>
                                            </button>
                                            <div class="dropdown-menu bg-light">
                                                <a class="dropdown-item"
                                                    href="{{ route('ulasan.edit', $rows->id_ulasan) }}"><i
                                                        class="fas fa-edit"></i> Ubah</a>
                                                <a onclick="event.preventDefault();
                                    document.getElementById('delete-ulasan').submit()"
                                                    class="dropdown-item"
                                                    href="{{ route('ulasan.destroy', $rows->id_ulasan) }}"><i
                                                        class="fas fa-trash"></i> Hapus</a>
                                                <form id="delete-ulasan"
                                                    action="{{ route('ulasan.destroy', $rows->id_ulasan) }}" role="form"
                                                    method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
