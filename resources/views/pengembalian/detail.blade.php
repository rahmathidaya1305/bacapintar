@section('title', 'Halaman Pengembalian Buku')
@section('breadcumb-title', 'Daftar Pengembalian Buku')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <form role="form" action="{{ route('return.insert', $peminjaman->id_peminjaman) }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id_peminjam" value="{{ $peminjaman->id_peminjaman }}">
                        <div class="col-md-6 ">
                            <div class="form-group">
                              <label class="text-muted" for="user">User</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="text" name="user" id="user" value="{{ ucwords($peminjaman->user->name) }}">
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="judul">Judul Buku</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="text" name="judul" id="judul" value="{{ $peminjaman->buku->judul }}">
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="tglpinjam">Tanggal Peminjaman</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="date" name="tglpinjam" id="tglpinjam" value="{{ $peminjaman->tgl_peminjaman }}">
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="tglkembali">Tanggal Pengembalian</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="date" name="tglkembali" id="tglkembali" value="{{ $peminjaman->tgl_pengembalian}}">
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="totalbuku">Total Buku</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="text" name="totalbuku" id="totalbuku" value="{{ $peminjaman->total_buku }}">
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="sewa">Harga Sewa</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="text" name="sewa" id="sewa" value="{{ $peminjaman->harga }}">
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="denda">Denda Keterlambatan</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="text" name="denda" id="denda" value="{{ $peminjaman->denda }}">
                               <small class="text-danger">Telat 1 hari * 2000</small>
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="status">Status</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="text" name="status" id="status" value="{{ $peminjaman->status}}">
                            </div>
                            <div class="form-group">
                              <label class="text-muted" for="totalbayar">Total Pembayaran</label>
                               <input readonly class="form-control border-0 p-0 bg-transparent" type="text" name="totalbayar" id="totalbayar" value="{{ $peminjaman->total_buku * $peminjaman->harga + $peminjaman->denda }}">
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
                        </div>
                      </form>
                </div>
            </div>

        </div>
    </div>
@endsection
