@section('title', 'Halaman Pengembalian')
@section('breadcumb-title', 'Daftar Kembali Buku')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                {{-- <div class="card-header">
                    <a class="btn btn-info btn-sm" href="#"><i class="fas fa-plus-circle"></i> </a>
                </div> --}}
                <div class="card-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User</th>
                                <th>Tgl. Pengembalian</th>
                                <th>Harga Sewa</th>
                                <th>Denda</th>
                                <th>Total Bayar</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($kembali as $rows)
                            <tr>
                                <td class="align-middle">{{ $loop->iteration }}</td>
                                <td class="align-middle">{{ $rows->peminjaman->user->name }}</td>
                                <td class="align-middle">{{ $rows->tgl_pengembalian }}</td>
                                <td class="align-middle">{{ "Rp. ".$rows->harga_sewa }}</td>
                                <td class="align-middle">{{ "Rp. ".$rows->denda }}</td>
                                <td class="align-middle">{{ "Rp. ".$rows->total_bayar }}</td>
                                <td class="align-middle">{{ $rows->status }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
