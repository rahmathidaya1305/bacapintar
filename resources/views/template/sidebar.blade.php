<style>
    .user-panel img {
      height: 2.1rem;
      width: 2.1rem;
      max-width: 2.1rem;
      max-height: 2.1rem;

  }
  .nav-sidebar .nav-header:not(:first-of-type) {
      padding: .5rem .5rem .5rem;
  }
  </style>
 <!-- Main Sidebar Container -->
 <aside class="main-sidebar sidebar-dark-primary elevation-4">
     <!-- Brand Logo -->
     <a href="#" class="brand-link">
         <img src="{{ asset('assets/icon/icon.jpg') }}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
         <span class="brand-text font-weight-light">BacaPintar</span>
     </a>

     <!-- Sidebar -->
     <div class="sidebar">
         <!-- Sidebar user panel (optional) -->
         <div class="user-panel mt-3 pb-3 mb-3 d-flex">
             <div class="image">
                 <img src="{{ asset('assets/image/'.Auth::user()->avatar) }}" class="img-circle elevation-2" alt="User Image">
             </div>
             <div class="info">
                 <a href="{{ route('user.index',['username'=>Auth::user()->username, 'id'=>Auth::user()->id_user]) }}" class="d-block">{{ ucwords(Auth::user()->name) }}</a>
             </div>
         </div>

         <!-- Sidebar Menu -->
         <nav class="mt-2">
             <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                 data-accordion="false">
                 <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                 <li class="nav-item">
                     <a href="{{ route('home') }}" class="nav-link">
                         <i class="nav-icon fas fa-tachometer-alt"></i>
                         <p>
                             Dashboard
                         </p>
                     </a>
                 </li>
                 <li class="nav-item has-treeview
                 @if (request()->is('category') || request()->is('books'))
                 menu-open
                 @endif
                 ">
                     <a href="#" class="nav-link
                        @if (request()->is('books') || request()->is('category'))
                        active
                        @endif
                        ">
                         <i class="nav-icon fas fa-book"></i>
                         <p>
                             Buku
                             <i class="fas fa-angle-left right"></i>
                             <span class="badge badge-info right">2</span>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a href="{{ route('category.index') }}" class="nav-link {{ request()->is('category') ? 'active' : '' }}">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Kategori Buku</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="{{ route('books.index') }}" class="nav-link {{ request()->is('books') ? 'active' : '' }}">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Daftar Buku</p>
                             </a>
                         </li>
                     </ul>
                 </li>

                 <li class="nav-item has-treeview
                 @if (request()->is('peminjaman') || request()->is('back'))
                 menu-open
                 @endif">
                     <a href="#" class="nav-link @if (request()->is('peminjaman') || request()->is('back'))
                        active
                        @endif">
                         <i class="nav-icon fas fa-file-invoice-dollar"></i>
                         <p>
                             Transaksi
                             <i class="right fas fa-angle-left"></i>
                             <span class="badge badge-info right">2</span>
                         </p>
                     </a>
                     <ul class="nav nav-treeview">
                         <li class="nav-item">
                             <a href="{{ route('peminjaman.index') }}" class="nav-link {{ request()->is('peminjaman') ? 'active' : '' }}">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Peminjaman</p>
                             </a>
                         </li>
                         <li class="nav-item">
                             <a href="{{ route('return.index') }}" class="nav-link {{ request()->is('back') ? 'active' : '' }}">
                                 <i class="far fa-circle nav-icon"></i>
                                 <p>Pengembalian Buku</p>
                             </a>
                         </li>
                     </ul>
                 </li>

                 <li class="nav-item">
                     <a href="{{ route('ulasan.index') }}" class="nav-link {{ request()->is('ulasan') ? 'active' : '' }}">
                         <i class="nav-icon fas fa-star"></i>
                         <p>
                             Ulasan
                         </p>
                     </a>
                 </li>


                 <li class="nav-header">SETTINGS</li>
                 <li class="nav-item">
                     <a href="#" class="nav-link">
                         <i class="nav-icon fas fa-user-cog"></i>
                         <p>
                             Akun
                         </p>
                     </a>
                 </li>

                 <li class="nav-item">
                     <a onclick="event.preventDefault();
                     document.getElementById('logout-form').submit();"
                         href="{{ route('logout') }}" class="nav-link">
                         <i class="nav-icon fas fa-sign-out-alt "></i>
                         <p>
                             Keluar
                         </p>
                     </a>
                     <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                 </li>
             </ul>
         </nav>
     </div>
 </aside>

