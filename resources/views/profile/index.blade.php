@section('title', 'Profile')
@section('breadcumb-title', 'Profile')
@extends('template.index')
@section('content')

    <style>
        .profile-user-img {
            border: 3px solid #adb5bd;
            margin: 0 auto;
            padding: 3px;
            width: 100px;
            height: 100px;
            max-width: 100px;
            max-height: 100px;
        }
    </style>
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
        </div>
        <div class="col-md-3">
            <div class="card card-primary card-outline">
                <div class="card-body box-profile">
                    <div class="text-center">
                        <img id="preview" class="profile-user-img img-fluid img-circle" src="{{ asset('assets/image/'.$user->avatar) }}"
                            alt="User profile picture">
                    </div>
                    <h3 class="profile-username text-center">{{ ucwords($user->name) }}</h3>
                    <input accept="image/*" type="file" class="d-none" name="photo" id="photo">
                    <label for="photo" class="btn btn-primary btn-block"><i class="fas fa-camera"></i> Select
                        File</label>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="card card-primary card-outline">
                <div class="card-body">
                    <form id="form-profile" action="{{ route('user.update', ['username' => $user->username, 'id' => $user->id_user]) }}"
                        method="post" role="form" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" id="name" name="name"
                                value="{{ ucwords($user->name) }}">
                        </div>

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input readonly type="text" class="form-control" id="username" name="username"
                                value="{{ $user->username }}">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input readonly type="email" class="form-control" id="email" name="email"
                                value="{{ $user->email }}">
                        </div>
                        <div id="path-photo" class="form-group"></div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary"><i class="fas fa-edit"></i> Update
                                Profile</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

@endsection
