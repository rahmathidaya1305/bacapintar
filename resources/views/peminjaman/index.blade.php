@section('title', 'Halaman Peminjaman')
@section('breadcumb-title', 'Daftar Peminjam')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-info btn-sm" href="{{ route('peminjaman.create') }}"><i class="fas fa-plus-circle"></i> Tambah
                        Peminjam</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User</th>
                                <th>Judul Buku</th>
                                <th>Tgl. Peminjaman</th>
                                <th>Tgl. Pengembalian</th>
                                <th>Harga Sewa</th>
                                <th>Total Buku</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($peminjaman as $rows)
                            <tr>
                                <td class="align-middle">{{ $loop->iteration }}</td>
                                <td class="align-middle">{{ ucwords($rows->user->name) }}</td>
                                <td class="align-middle">{{ $rows->buku->judul  }}</td>
                                <td id="tgl_pinjam" class="align-middle">{{ $rows->tgl_peminjaman }}</td>
                                <td id="tgl_kembali" class="align-middle">{{ $rows->tgl_pengembalian }}</td>
                                <td class="align-middle">{{ "Rp. ".$rows->buku->harga }}</td>
                                <td class="align-middle">{{ $rows->total_buku }}</td>
                                <td class="align-middle status">
                                    @if ($rows->tgl_pengembalian < date('Y-m-d'))
                                    {{ $rows->status = 'terlambat' }}
                                    @else
                                    {{ $rows->status }}
                                    @endif
                                </td>
                                <td class="align-middle">
                                    <div class="btn-group dropleft">
                                        <button type="button" class="btn btn-primary dropdown-toggle"
                                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <i class="fas fa-eye"></i>
                                        </button>
                                        <div class="dropdown-menu bg-light">
                                            @if ($rows->status == 'dikembalikan')
                                            <a hidden class="dropdown-item" href="{{ route('return.back', $rows->id_peminjaman) }}"><i class="fas fa-sync" ></i> Kembalikan</a>
                                            @else
                                            <a class="dropdown-item" href="{{ route('return.back', $rows->id_peminjaman) }}"><i class="fas fa-sync" ></i> Kembalikan</a>
                                            @endif
                                            <a class="dropdown-item" href="{{route('peminjaman.edit', $rows->id_peminjaman)}}"><i class="fas fa-edit" ></i> Ubah</a>
                                            <form method="POST" action="{{route('peminjaman.destroy', $rows->id_peminjaman)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="dropdown-item"><i class="fas fa-trash" ></i> Hapus</button>
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
