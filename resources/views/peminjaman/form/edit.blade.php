@section('title', 'Halaman Ubah Peminjaman')
@section('breadcumb-title', 'Ubah Peminjaman')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if ($message = Session::get('success'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    <strong>{{ $message }}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title font-weight-bold"> <i class="fas fa-file"></i> Form Ubah Peminjaman</h3>
                </div>
                <div class="card-body">
                    <form action="{{ route('peminjaman.update', $peminjaman->id_peminjaman) }}" method="post" role="form"
                        enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col mb-3">
                                <label for="user">User</label>
                                <select class="form-control" name="user" id="user">
                                    @foreach ($user as $row)
                                        <option value="{{ $row->id_user }}"
                                            @if ($row->id_user === $peminjaman->id_user)
                                            @selected(true)
                                        @endif>{{ ucwords($row->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col mb-3">
                                <label for="buku">Judul Buku</label>
                                <select class="form-control" name="buku" id="buku">
                                    <option value="">--Pilih Judul Buku--</option>
                                    @foreach ($buku as $rows)
                                        <option data-harga="{{ $rows->harga }}" value="{{ $rows->id_buku }}"
                                            @if ($rows->id_buku === $peminjaman->id_buku)
                                                @selected(true)
                                            @endif>
                                            {{ $rows->judul }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col mb-3">
                                <label for="tglpinjam">Tanggal Peminjaman</label>
                                <input class="form-control" type="date" value="{{ $peminjaman->tgl_peminjaman }}" name="tglpinjam" id="tglpinjam">
                            </div>
                            <div class="col mb-3">
                                <label for="tglkembali">Tanggal Pengembalian</label>
                                <input class="form-control" type="date" value="{{ $peminjaman->tgl_pengembalian }}" name="tglkembali" id="tglkembali">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col mb-3">
                                <label for="hargasewa">Harga Sewa</label>
                                <input readonly class="form-control" type="text" value="{{ $peminjaman->harga }}" name="hargasewa" id="hargasewa">
                            </div>
                            <div class="col mb-3">
                                <label for="ttlbuku">Total Buku</label>
                                <select class="form-control" name="ttlbuku" id="ttlbuku">
                                    <option selected style="font-weight: bold" value="{{ $peminjaman->total_buku }}">{{ "Total Buku Pinjam - ". $peminjaman->total_buku }}</option>
                                    <option  value="0">Pilih Jumlah Buku</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6 mb-3">
                                <label for="status">Status</label>
                                <select class="form-control" name="status" id="status">
                                    <option style="font-weight: bold" value="{{ $peminjaman->status }}">{{ "Status Saat ini ".$peminjaman->status }}</option>
                                    <option value="dipinjam">Dipinjam</option>
                                    <option value="dikembalikan">Dikembalikan</option>
                                    <option value="terlambat">Terlambat</option>
                                    <option value="diperpanjang">Diperpanjang</option>
                                    <option value="dibatalkan">Dibatalkan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <button type="reset" class="btn btn-warning"><i class="fas fa-sync-alt"></i> Hapus</button>
                            <button type="submit" class="btn btn-info"><i class="fas fa-edit"></i> Ubah</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
