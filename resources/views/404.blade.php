@include('template.header')
<div class="content pt-5 mt-5">
    <!-- Main content -->
    <section class="content">
        <div class="error-page">
            <h2 class="headline text-warning"> 404</h2>

            <div class="error-content">
                <h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Page not found.</h3>
                {{-- fitur ini belu ready --}}
                {{-- <p>
                    We could not find the page you were looking for.
                    Meanwhile, you may <a href="#">return to dashboard</a> or try using the search form.
                </p> --}}

            </div>
            <!-- /.error-content -->
        </div>
        <a class="link-danger" href="{{ route('logout') }}"
            onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </section>
    <!-- /.content -->
</div>
@include('template.footer')
