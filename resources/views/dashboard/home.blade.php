@section('title', 'Dashboard')
@section('breadcumb-title', 'Dashboard')
@extends('template.index')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="jumbotron">
                <h1 class="display-4">Selamat Datang, {{ ucwords(Auth::user()->name) }}</h1>
                <p class="lead">Anda telah masuk sebagai Administrator. Kami berharap Anda memiliki hari yang produktif dan
                    sukses!</p>
                <hr class="my-4">
                <p>Gunakan Aplikasi ini baik untuk mengelola Buku dan Transaksi peminjaman dan pengaturan situs web dengan mudah</p>
                {{-- <a class="btn btn-primary btn-lg" href="{{ route('home') }}" role="button">Mulai</a> --}}
            </div>
        </div>
    </div>
@endsection
